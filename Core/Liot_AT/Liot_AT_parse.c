#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "Liot_AT.h"

uint8_t AT_find_char_num(char *str, char value)
{
    uint8_t num;
    uint16_t i;

    num = 0;

    if(str == NULL)
    {
        return num;
    }
    for(i = 0; str[i] != '\0'; i++)
    {
        if(str[i] == value)
        {
            num++;
        }
    }
    return num;
}

AT_errorcode_t Liot_AT_rsp_check_keyword(uint8_t *rsp, uint8_t *key)
{
    uint8_t *offset = NULL;

    if(rsp == NULL || key == NULL)
    {
        return -1;
    }
    offset = strstr(rsp, key);
    if(offset == NULL)
    {
        return AT_RECV_NO_KEYWORD;
    }
    else
    {
        return AT_OK;
    }
}

uint8_t Liot_AT_rsp_get_param_num(uint8_t *rsp, uint8_t *key)
{
    uint8_t param_num;
    uint8_t *offset = NULL;

    if(rsp == NULL || key == NULL)
    {
        return -1;
    }
    offset = strstr(rsp, key);
    if(offset == NULL)
    {
        AT_LOGE("No keyword : %s found in reply\r\n", key);
        return -1;
    }

    param_num = AT_find_char_num(&offset[2], ',') + 1;

    return param_num;
}

AT_errorcode_t Liot_AT_rsp_get_param(uint8_t *rsp, uint8_t *key, uint8_t num, Liot_AT_param_type_t type, void *output)
{
    uint8_t *offset = NULL;
    uint8_t *next = NULL;
    uint8_t *last = NULL;
    uint8_t param_num;
    uint8_t i;

    uint8_t *buf = NULL;
    uint8_t len = 0;

    if(output == NULL || rsp == NULL || key == NULL)
    {
        return AT_INVALID_PARAM;
    }

    offset = strstr(rsp, key);
    if(offset == NULL)
    {
        AT_LOGE("No keyword : %s found in reply\r\n", key);
        return AT_RECV_NO_KEYWORD;
    }

    param_num = AT_find_char_num(&offset[2], ',') + 1;

    if(num > param_num)
    {
        AT_LOGE("Receive ERROR param in reply\r\n");
        return AT_RECV_ERROR_PARAM;
    }

    offset = strstr(rsp, ":");
    offset = &offset[1];
    last = offset;
    next = offset;
    for(i = 0 ; i < (num + 1) ; i++)
    {
        last = offset + 1;
        next = strstr(last, ",");
        if(next == NULL)
        {
            next = strstr(last, "\r\n");
//            next = last + strlen(last);
            break;
        }
        offset = next;
    }

    len = next - last + 1;
    buf = malloc(len);
    if(buf == NULL)
    {
        return AT_NO_MEM;
    }
    memset(buf, 0, sizeof(buf));
    memcpy(buf, last, (next - last) );
    switch(type)
    {
        case AT_PARAM_TYPE_STR:
        {
            if(buf[0] == '\"' && buf[len - 2] == '\"')
            {
                memcpy((uint8_t *)output, &buf[1], len - 3);
            }
            else
            {
                memcpy((uint8_t *)output, buf, len);
            }
            break;
        }
        case AT_PARAM_TYPE_HEX:
        {
            memcpy((uint8_t *)output, buf, sizeof(buf));
            break;
        }
        case AT_PARAM_TYPE_DEC:
        {
            (*(int32_t *)output) = atol(buf);
            break;
        }
        default:break;
    }

    free(buf);

    return AT_OK;

}
