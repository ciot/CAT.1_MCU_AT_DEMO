#ifndef _LIOT_AT_TCPIP_H_
#define _LIOT_AT_TCPIP_H_

#include "Liot_AT.h"

#define SOCKET_ID_NUM   7

typedef enum
{
    AT_SOCKET_URC_RECV_DATA = 0,
    AT_SOCKET_URC_RECV_ERROR,
}Liot_AT_socket_urc_event_t;

typedef enum
{
    AT_SOCKET_TYPE_TCP = 0,
    AT_SOCKET_TYPE_UDP,
}Liot_AT_socket_type_t;

typedef enum
{
    AT_SOCKET_BUFFER_MODE = 0,
    AT_SOCKET_PUSH_MODE,
    AT_SOCKET_TRANSPARENT,  //�ݲ�֧��
}Liot_AT_socket_mode_t;

typedef enum
{
    AT_SOCKET_INITIAL = 0,
    AT_SOCKET_CONNECTING,
    AT_SOCKET_CONNECTED,
    AT_SOCKET_CLOSING,
    AT_SOCKET_REMOTE_CLOSING,
}Liot_AT_socket_status_t;

typedef struct
{
    uint8_t enable;
    Liot_AT_socket_type_t type;
    uint8_t host[150];
    uint16_t port;
    uint16_t local_port;
    Liot_AT_socket_status_t socket_status;
    Liot_AT_socket_mode_t access_mode;
}Liot_AT_tcp_status_t;

typedef struct
{
    uint8_t host[150];
    uint16_t port;
    Liot_AT_socket_mode_t mode;
}Liot_AT_tcp_cfg_t;

typedef void (*socket_LIPURC_callback_fun_t)(uint8_t socket_id, Liot_AT_socket_urc_event_t event, uint8_t *data, uint16_t *len);

uint8_t Liot_check_socket_status(void);
AT_errorcode_t Liot_AT_get_socket_status(void);
AT_errorcode_t Liot_AT_tcp_connect(Liot_AT_tcp_cfg_t *cfg, int8_t *id);
AT_errorcode_t Liot_AT_tcp_close(uint8_t socket_id);
AT_errorcode_t Liot_AT_tcp_send(uint8_t socket_id, uint8_t *data, uint16_t len);
AT_errorcode_t Liot_AT_tcp_read(uint8_t socket_id, uint8_t *data, uint16_t maxlen, uint16_t *rlen);



#endif // _LIOT_AT_TCPIP_H_

