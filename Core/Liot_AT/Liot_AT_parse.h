#ifndef _LIOT_AT_PARSE_H_
#define _LIOT_AT_PARSE_H_

#include "Liot_AT.h"

typedef enum
{
    AT_PARAM_TYPE_STR,
    AT_PARAM_TYPE_HEX,
    AT_PARAM_TYPE_DEC,
    AT_PARAM_TYPE_JSON,
}Liot_AT_param_type_t;

uint8_t AT_find_char_num(char *str, char value);
AT_errorcode_t Liot_AT_rsp_check_keyword(uint8_t *rsp, uint8_t *key);
AT_errorcode_t Liot_AT_rsp_get_param(uint8_t *rsp, uint8_t *key, uint8_t num, Liot_AT_param_type_t type, void *output);

#endif

