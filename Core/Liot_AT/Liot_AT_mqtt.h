#ifndef _LIOT_AT_MQTT_H_
#define _LIOT_AT_MQTT_H_

#include "Liot_AT.h"

typedef struct _Liot_AT_mqtt_cfg_t
{
    uint8_t host[100];
    uint16_t port;
}Liot_AT_mqtt_cfg_t;

typedef struct _Liot_AT_mqtt_conn_cfg_t
{
    uint8_t clientID[256];
    uint8_t username[256];
    uint8_t password[256];
}Liot_AT_mqtt_conn_cfg_t;

typedef struct _Liot_AT_mqtt_topic_cfg_t
{
    uint16_t msgid;
    uint8_t topic[256];
    uint8_t qos;
}Liot_AT_mqtt_topic_cfg_t;

typedef enum _Liot_AT_mqtt_urc_event_t
{
    AT_MQTT_URC_STATS = 0,
    AT_MQTT_URC_RECV,
}Liot_AT_mqtt_urc_event_t;

AT_errorcode_t Liot_AT_mqtt_open(uint8_t id, Liot_AT_mqtt_cfg_t *cfg, int8_t *result);
AT_errorcode_t Liot_AT_mqtt_close(uint8_t id, uint8_t *result);
AT_errorcode_t Liot_AT_mqtt_connect(uint8_t id, Liot_AT_mqtt_conn_cfg_t *cfg, uint8_t *result);
AT_errorcode_t Liot_AT_mqtt_disconnect(uint8_t id, uint8_t *result);
AT_errorcode_t Liot_AT_mqtt_subscribe(uint8_t id, Liot_AT_mqtt_topic_cfg_t *cfg, uint8_t *result);
AT_errorcode_t Liot_AT_mqtt_unsubscribe(uint8_t id, Liot_AT_mqtt_topic_cfg_t *cfg, uint8_t *result);
AT_errorcode_t Liot_AT_mqtt_publish(uint8_t id, Liot_AT_mqtt_topic_cfg_t *cfg, uint8_t *data, uint16_t len, uint8_t retain, uint8_t *result);
AT_errorcode_t Liot_AT_mqtt_publishex(uint8_t id, Liot_AT_mqtt_topic_cfg_t *cfg, uint8_t *data, uint16_t len, uint8_t retain, uint32_t interval, uint8_t *result);
AT_errorcode_t Liot_AT_mqtt_read(uint32_t *id, uint32_t *msgid, uint8_t *topic, uint8_t *data, uint32_t *len);


#endif


