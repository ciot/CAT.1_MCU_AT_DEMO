#ifndef _LIOT_AT_H_
#define _LIOT_AT_H_

#include "main.h"
#include "Liot_AT_port.h"
#include "Liot_AT_parse.h"
#include "Liot_AT_tcpip.h"
#include "Liot_AT_mqtt.h"



typedef struct
{
    uint8_t manufacturer[16];
    uint8_t product_name[16];
    uint8_t version[32];
}Liot_product_info_t;

typedef enum
{
    AT_CMEE_DISABLE,
    AT_CMEE_BYTE,
    AT_CMEE_STRING,
    AT_CMEE_MAX = AT_CMEE_STRING,
}Liot_AT_CMEE_type_t;

void Liot_AT_get_rsp_error_reason(uint8_t *rsp);

AT_errorcode_t Liot_get_imei(uint8_t *imei);
AT_errorcode_t Liot_set_cfun(uint8_t cfun);
AT_errorcode_t Liot_set_CMEE(Liot_AT_CMEE_type_t type);
AT_errorcode_t Liot_get_CMEE(void);
AT_errorcode_t Liot_get_iccid(uint8_t *iccid);
AT_errorcode_t Liot_get_cereg(uint8_t *cereg);
AT_errorcode_t Liot_AT_module_reset(void);

#endif // _LIOT_AT_H_
