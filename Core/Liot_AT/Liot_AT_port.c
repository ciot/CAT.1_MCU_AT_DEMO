#include <stdio.h>
#include <stdint.h>

#include "Liot_AT.h"
#include "main.h"

extern uint8_t AT_rsp_status;

void AT_delay_ms(uint32_t ms)
{
    HAL_Delay(ms);
}

void Liot_AT_cmd_send(uint8_t *data, uint16_t len)
{
		HAL_UART_Transmit(&huart1, (uint8_t *)data, len, 0xFFFF);
		LOGT("%s\n",data);
}

void Liot_AT_cmd_recv(uint8_t *data, uint16_t *len)
{
    HAL_UART_Receive(&huart1, (uint8_t *)data, *len, 0xFFFF);
}

AT_errorcode_t Liot_AT_wait_rsp(uint32_t ms)
{
    AT_rsp_status = 0;
    do
    {
        AT_delay_ms(1);
        ms--;
    }while(ms && (AT_rsp_status == 0));

    if(AT_rsp_status == 1)
    {
        return AT_OK;
    }
    else if(ms == 0)
    {
        return AT_TIMEOUT;
    }
}








