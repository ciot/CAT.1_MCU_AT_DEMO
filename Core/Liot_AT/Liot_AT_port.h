#ifndef _LIOT_AT_PORT_H_
#define _LIOT_AT_PORT_H_

#include "Liot_AT.h"

#define AT_RSP_SIZE 2048

#define AT_LOG_EN 0

#if AT_LOG_EN
#define AT_LOGE(format, ...)        LOGE(format, ##__VA_ARGS__)
#define AT_LOGW(format, ...)        LOGW(format, ##__VA_ARGS__)
#define AT_LOGI(format, ...)        LOGI(format, ##__VA_ARGS__)
#define AT_LOGD(format, ...)        LOGD(format, ##__VA_ARGS__)
#define AT_LOG(...)                 printf(__VA_ARGS__)
#else
#define AT_LOGE(format, ...)
#define AT_LOGW(format, ...)
#define AT_LOGI(format, ...)
#define AT_LOGD(format, ...)
#define AT_LOG(format, ...)
#endif

typedef enum
{
    AT_OK,
    AT_INVALID_PARAM,
    AT_TIMEOUT,
    AT_NO_MEM,
    AT_RECV_ERROR,
    AT_RECV_NO_KEYWORD,
    AT_RECV_ERROR_PARAM,
}AT_errorcode_t;

extern uint8_t g_at_rsp[AT_RSP_SIZE];

void AT_delay_ms(uint32_t ms);

void Liot_AT_cmd_send(uint8_t *data, uint16_t len);
void Liot_AT_cmd_recv(uint8_t *data, uint16_t *len);
AT_errorcode_t Liot_AT_wait_rsp(uint32_t ms);

#endif // _LIOT_AT_PORT_H_
