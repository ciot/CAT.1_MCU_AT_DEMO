/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdarg.h>
#include "usart.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#define LOG_TXRX_EN     1

#define LOG_ERROR       0
#define LOG_WARING      1
#define LOG_INFO        2
#define LOG_DEBUG       3

#define LOG_TX          20
#define LOG_RX          21

#define LOG_MAX_LEN 512
#define LOG_PRINT(...)          printf(__VA_ARGS__)

#define LOG(...)                printf(__VA_ARGS__)
#define LOGI(format, ...)       Liot_log(LOG_INFO, format, ##__VA_ARGS__)
#define LOGW(format, ...)       Liot_log(LOG_WARING, format, ##__VA_ARGS__)
#define LOGE(format, ...)       Liot_log(LOG_ERROR, format, ##__VA_ARGS__)
#define LOGD(format, ...)       Liot_log(LOG_DEBUG, format, ##__VA_ARGS__)

#if LOG_TXRX_EN
#define LOGT(format, ...)       Liot_log(LOG_TX, format, ##__VA_ARGS__)
#define LOGR(format, ...)       Liot_log(LOG_RX, format, ##__VA_ARGS__)
#else
#define LOGT(format, ...)
#define LOGR(format, ...)
#endif
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MCO_Pin GPIO_PIN_0
#define MCO_GPIO_Port GPIOF
#define USART2_TX_Pin GPIO_PIN_2
#define USART2_TX_GPIO_Port GPIOA
#define USART2_RX_Pin GPIO_PIN_3
#define USART2_RX_GPIO_Port GPIOA
#define LED_GREEN_Pin GPIO_PIN_5
#define LED_GREEN_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
